import React, { Component } from "react";
import image3 from "../assets/image/image3.webp";
import image4 from "../assets/image/image4.webp";
import image5 from "../assets/image/image5.webp";
import image6 from "../assets/image/image6.webp";

export default class CarouselPhoto extends Component {
  render() {
    return (
      <div>
        <div
          id="carouselExampleSlidesOnly"
          class="carousel slide"
          data-ride="carousel"
        >
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src={image3} class="d-block w-100" alt="..." />
            </div>
            <div class="carousel-item">
              <img src={image4} class="d-block w-100" alt="..." />
            </div>
            <div class="carousel-item">
              <img src={image5} class="d-block w-100" alt="..." />
            </div>
            <div class="carousel-item">
              <img src={image6} class="d-block w-100" alt="..." />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
