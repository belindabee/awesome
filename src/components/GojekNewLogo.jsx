import React, { Component } from "react";
import image2 from "../assets/image/image2.webp";

export default class GojekNewLogo extends Component {
  render() {
    return (
      <div>
        <div className="gojek">
          <img src={image2} alt="logos" />
        </div>
        <div className="meet">
          <h1>
            Meet Solv, Gojek's new <br />
            logo
          </h1>
          <p>
            Our new logo symbolize Gojek's tranformation from being a <br />
            ride-hailing service to becaoming the largest Super App, with a{" "}
            <br />
            variety of smart ways to eliminate hassies{" "}
          </p>
        </div>
        <div className="arrow">
          <h4>Find Out Here</h4>
        </div>
        <div className="arrow-1">
          <i className="fa fa-arrow-right"></i>
        </div>
      </div>
    );
  }
}
