import React, { Component } from "react";
import image1 from "../assets/image/image1.jpg";

export default class JumbotronPhoto extends Component {
  render() {
    return (
      <div>
        <div className="photo">
          <img src={image1} alt="photos" />
        </div>
        <div className="word">
          <h1>
            The solution for every
            <br />
            situation.
          </h1>
          <p>With Gojek, there is always a way</p>
        </div>
      </div>
    );
  }
}
