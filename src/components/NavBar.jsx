import React, { Component } from "react";
import logo from "../assets/image/logo.svg";
import "../assets/asset.css";

export default class NavBar extends Component {
  render() {
    return (
      <div className="container">
        <div className="logo">
          <img src={logo} alt="logo" />
          <ul>
            <li>
              <a href="#">About Us</a>
            </li>
            <li>
              <a href="#">Services</a>
            </li>
            <li>
              <a href="#">Blog</a>
            </li>
            <li>
              <a href="#">Help Center</a>
            </li>
            <li>
              <a href="#">Join Us</a>
            </li>
            <li>
              <a href="#">Safety</a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
