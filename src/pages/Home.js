import React from "react";
import NavBar from "../components/NavBar";
import JumbotronPhoto from "../components/JumbotronPhoto";
import GojekNewLogo from "../components/GojekNewLogo";
import CarouselPhoto from "../components/CarouselPhoto";

export default class Home extends React.Component {
  render() {
    return (
      <div>
        <NavBar />
        <JumbotronPhoto />
        <GojekNewLogo />
        <CarouselPhoto />
      </div>
    );
  }
}
